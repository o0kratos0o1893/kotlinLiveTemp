**Class:**  
**extadapter**: gen extrecycleview adapter  
**baseAdapter**: gen baseRecycle adapter  
**alone**:  AloneFragmentActivity.with(context!!).start(::class.java)  
**aloneparam**:  AloneFragmentActivity.with(context!!).parameters().start(::class.java)  
**contructorCustom**: gen contructor custom view  
**initToolbar**: gen toolbar back button, ect...  
**newIns**: gen newinstance bundle  
**getView**: gen request api code  
**funU**: gen function return Unit  
**funI**: gen function return Int  
**funS**: gen function return String  
**funB**: ge function return Boolean  
**registerBusActi**: gen function register rxbus in Activity  
**registerBusFragment**: gen function register rxbus in Fragment  


**XML: ** 
**bggradient**: gen shape with gradient  
**bgboder**: gen shape with border  
**bgconner**: gen shape with conner radius  

Android studio -> import setting -> import my file -> restart Android studio
